package com.brain.gitrepo.utils

object Utils {

    const val TAG = "SEARCH_GIT"
    const val BASE_URL = "https://api.github.com/"
    const val PACKAGE = "com.brain.gitrepo"
    const val KEY_IMAGE_URL = "image_url"
    const val KEY_NAME = "name"
    const val KEY_DESCRIPTION = "description"
    const val KEY_UPDATED_DATE = "updated_date"
    const val KEY_REPO_URL = "html_url"
}