package com.brain.gitrepo.view

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.brain.gitrepo.R
import com.brain.gitrepo.adapter.AdapterRepoItem
import com.brain.gitrepo.viewmodel.SearchViewModel
import com.brain.gitrepo.databinding.FragmentRepoListBinding
import com.brain.gitrepo.models.Item
import com.brain.gitrepo.utils.NetworkResult
import com.brain.gitrepo.utils.Utils.KEY_DESCRIPTION
import com.brain.gitrepo.utils.Utils.KEY_IMAGE_URL
import com.brain.gitrepo.utils.Utils.KEY_NAME
import com.brain.gitrepo.utils.Utils.KEY_REPO_URL
import com.brain.gitrepo.utils.Utils.KEY_UPDATED_DATE
import com.brain.gitrepo.utils.Utils.TAG
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RepoListFragment : Fragment(), AdapterRepoItem.OnItemClickListener {

    private var _binding: FragmentRepoListBinding? = null
    private val binding get() = _binding!!

    private lateinit var recyclerRepoList: RecyclerView
    private lateinit var repoItemAdapter: AdapterRepoItem

    private val searchViewModel by viewModels<SearchViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (isNetworkConnected()){
            searchViewModel.searchRepositories("Android", "star", 1)
        }else{
            showToast("No internet connection")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        _binding = FragmentRepoListBinding.inflate(inflater, container, false)

        recyclerRepoList = binding.recyclerRepoList
        recyclerRepoList.layoutManager = LinearLayoutManager(context)
        repoItemAdapter = AdapterRepoItem(emptyList(), this)
        recyclerRepoList.adapter = repoItemAdapter

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        searchViewModel.searchResponseLiveData.observe(viewLifecycleOwner) {
            binding.loading.isVisible = false
            when (it) {
                is NetworkResult.Success -> {
                    Log.d(TAG, "onViewCreated: ${it.data}")
                    it.data?.let { it1 -> repoItemAdapter.updateData(it1.items) }
                }

                is NetworkResult.Error -> {
                    binding.loading.visibility = View.GONE
                    Toast.makeText(context, it.message, Toast.LENGTH_LONG).show()
                }

                is NetworkResult.Loading -> {
                    binding.loading.isVisible = true
                }
            }
        }
    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager = requireContext().getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
        if (connectivityManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val network = connectivityManager.activeNetwork ?: return false
                val networkCapabilities = connectivityManager.getNetworkCapabilities(network) ?: return false
                return networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            } else {
                val networkInfo = connectivityManager.activeNetworkInfo
                return networkInfo != null && networkInfo.isConnected
            }
        }
        return false
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun onItemClick(item: Item) {
        if (isNetworkConnected()){
            val bundle = Bundle()
            bundle.putString(KEY_IMAGE_URL,item.owner.avatar_url)
            bundle.putString(KEY_NAME,item.owner.login)
            bundle.putString(KEY_DESCRIPTION,item.description)
            bundle.putString(KEY_UPDATED_DATE,item.updated_at)
            bundle.putString(KEY_REPO_URL,item.html_url)
            findNavController().navigate(R.id.itemDetailsFragment, bundle)
        }else{
            showToast("No internet connection")
        }
    }
}