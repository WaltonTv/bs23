package com.brain.gitrepo.view

import android.content.Intent
import android.health.connect.datatypes.units.Length
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.brain.gitrepo.R
import com.brain.gitrepo.databinding.FragmentItemDetailsBinding
import com.brain.gitrepo.utils.Utils.KEY_DESCRIPTION
import com.brain.gitrepo.utils.Utils.KEY_IMAGE_URL
import com.brain.gitrepo.utils.Utils.KEY_NAME
import com.brain.gitrepo.utils.Utils.KEY_REPO_URL
import com.brain.gitrepo.utils.Utils.KEY_UPDATED_DATE
import com.bumptech.glide.Glide
import java.text.SimpleDateFormat
import java.util.Locale

class ItemDetailsFragment : Fragment() {

    private var _binding: FragmentItemDetailsBinding? = null
    private val binding get() = _binding!!

    private lateinit var updatedDate: String
    private lateinit var description: String
    private lateinit var imageUrl: String
    private lateinit var repoUrl: String
    lateinit var name: String


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentItemDetailsBinding.inflate(inflater, container, false)

        updatedDate = arguments?.getString(KEY_UPDATED_DATE).toString()
        description = arguments?.getString(KEY_DESCRIPTION).toString()
        imageUrl = arguments?.getString(KEY_IMAGE_URL).toString()
        repoUrl = arguments?.getString(KEY_REPO_URL).toString()
        name = arguments?.getString(KEY_NAME).toString()


        binding.apply {
            Glide.with(requireContext())
                .load(imageUrl)
                .placeholder(R.drawable.ic_image)
                .error(R.drawable.ic_image)
                .into(imgOwner)

            txtLastUpdateDateTime.text =
                updatedDate?.let { "Last Updated Date: ${formatDateTime(it)}" }
            txtRepoDescription.text = description
            txtRepoOwnerName.text = name
            txtUrl.text = repoUrl

            txtUrl.setOnClickListener {
                openUrlInBrowser(repoUrl)
            }
        }

        return binding.root
    }


    private fun formatDateTime(dateTimeString: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
        val outputFormat = SimpleDateFormat("MM-dd-yyyy HH:mm", Locale.getDefault())

        val date = inputFormat.parse(dateTimeString)
        return outputFormat.format(date)
    }

    private fun openUrlInBrowser(url: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}