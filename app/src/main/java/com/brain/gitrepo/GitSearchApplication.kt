package com.brain.gitrepo

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class GitSearchApplication : Application() {
}