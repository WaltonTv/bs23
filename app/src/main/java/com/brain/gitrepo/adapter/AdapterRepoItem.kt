package com.brain.gitrepo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.brain.gitrepo.R
import com.brain.gitrepo.models.Item

class AdapterRepoItem(private var dataList: List<Item>, private val listener: OnItemClickListener) :
    RecyclerView.Adapter<AdapterRepoItem.ViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(item: Item)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val txtRepoName: TextView = view.findViewById(R.id.txtRepoName)
        val txtRepoDetails: TextView = view.findViewById(R.id.txtRepoDetails)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.repo_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val repoItem = dataList[position]
        holder.txtRepoName.text = repoItem.name
        holder.txtRepoDetails.text = repoItem.description

        holder.itemView.setOnClickListener {
            listener.onItemClick(repoItem)
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun updateData(newData: List<Item>) {
        dataList = newData
        notifyDataSetChanged()
    }
}