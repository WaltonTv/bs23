package com.brain.gitrepo.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.brain.gitrepo.api.ISearchApi
import com.brain.gitrepo.models.SearchResponse
import com.brain.gitrepo.utils.NetworkResult
import org.json.JSONObject
import retrofit2.Response
import javax.inject.Inject

class SearchRepository @Inject constructor(private val searchApi: ISearchApi) {

    private val _searchResponseLiveData = MutableLiveData<NetworkResult<SearchResponse>>()
    val searchResponseLiveData : LiveData<NetworkResult<SearchResponse>>
        get() = _searchResponseLiveData

    suspend fun searchRepositories(searchQuery: String, sort: String, page: Int){
        _searchResponseLiveData.postValue(NetworkResult.Loading())
        val response = searchApi.searchRepositories(searchQuery, sort, page)
        handleResponse(response)
    }

    private fun handleResponse(response: Response<SearchResponse>) {
        if (response.isSuccessful && response.body() != null) {
            _searchResponseLiveData.postValue(NetworkResult.Success(response.body()!!))
        } else if (response.errorBody() != null) {
            val errorObj = JSONObject(response.errorBody()!!.charStream().readText())
            _searchResponseLiveData.postValue(NetworkResult.Error(errorObj.getString("message")))
        } else {
            _searchResponseLiveData.postValue(NetworkResult.Error("Something went wrong"))
        }
    }
}