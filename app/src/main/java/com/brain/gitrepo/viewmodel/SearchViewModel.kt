package com.brain.gitrepo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.brain.gitrepo.api.ISearchApi
import com.brain.gitrepo.models.SearchResponse
import com.brain.gitrepo.repository.SearchRepository
import com.brain.gitrepo.utils.NetworkResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(private val searchRepository: SearchRepository) : ViewModel(){

    val searchResponseLiveData : LiveData<NetworkResult<SearchResponse>>
        get() = searchRepository.searchResponseLiveData

    fun searchRepositories(searchQuery: String, sort: String, page: Int){
        viewModelScope.launch {
            searchRepository.searchRepositories(searchQuery, sort, page)
        }
    }

}