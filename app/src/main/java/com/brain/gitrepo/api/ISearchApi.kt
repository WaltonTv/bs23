package com.brain.gitrepo.api

import com.brain.gitrepo.models.SearchResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ISearchApi {

    @GET("search/repositories")
    suspend fun searchRepositories(
        @Query("q") query: String = "Android",
        @Query("sort") sort: String = "star",
        @Query("page") page: Int = 1,
        @Query("per_page") per_page: Int = 50
    ): Response<SearchResponse>

}