package com.brain.gitrepo.api

import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class PackageIdInterceptor @Inject constructor(private val packageId: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()
            .header("package", packageId)
            .build()
        return chain.proceed(request)
    }
}
